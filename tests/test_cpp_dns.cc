
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>
#include <boost/thread.hpp>
#include <boost/atomic.hpp>
#include <boost/chrono.hpp>
#include <string>

#include "dim.hxx"
#include "dis.hxx"
#include "dic.hxx"

#include "config.h"
#include "test_utils.hh"

#define EQ CPPUNIT_ASSERT_EQUAL
#define EQ_DBL CPPUNIT_ASSERT_DOUBLES_EQUAL

class TestDNS : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDNS);
    CPPUNIT_TEST(dns_close);
    CPPUNIT_TEST_SUITE_END();

    pid_t m_dns;

public:

    void setUp() {
        m_dns = -1;
    }

    void tearDown()
    {
        if (m_dns > 0)
        {
            terminate(m_dns);
            m_dns = -1;
        }

        DimServer::stop();
    }

    void dns_close()
    {
        {
            /* dns is offline */
            DimCurrentInfo info("TEST", -42);
            EQ(-42, info.getInt());
        }
        dic_stop();

        m_dns = spawn(BUILDDIR "/src/dns", "-d");
        CPPUNIT_ASSERT(m_dns >= 0);
        boost::this_thread::sleep_for(boost::chrono::seconds(3));

        {
            /* dns is now online */
            DimService serv("TEST", 1);
            DimServer::start("testServer");

            DimCurrentInfo info("TEST", -42);
            EQ(1, info.getInt());
        }
    }

};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDNS);
