

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>
#include <boost/thread.hpp>
#include <boost/atomic.hpp>
#include <boost/chrono.hpp>
#include <string>

#include "dim.hxx"
#include "dis.hxx"
#include "dic.hxx"

#include "config.h"
#include "test_utils.hh"

#define EQ CPPUNIT_ASSERT_EQUAL
#define EQ_DBL CPPUNIT_ASSERT_DOUBLES_EQUAL

class TestBasic : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestBasic);
    CPPUNIT_TEST(basic_types);
    CPPUNIT_TEST_SUITE_END();

    pid_t m_dns;

public:
    void setUp()
    {
        m_dns = spawn(BUILDDIR "/src/dns", "-d");
        CPPUNIT_ASSERT(m_dns >= 0);
        boost::this_thread::sleep_for(boost::chrono::seconds(3));
    }

    void tearDown()
    {
        terminate(m_dns);
        m_dns = -1;

        DimServer::stop();
    }

    void basic_types()
    {
        DimServer::start("testServer");
        type_int();
        type_float();
    }

    void type_int()
    {
        DimService serv("TEST", 1);
        EQ(0, serv.updateService(12));

        DimInfoWaiter info("TEST", -1);
        EQ(true, info.waitUpdate());
        EQ(12, info.getInt());

        info.reset();
        EQ(1, serv.updateService(13));
        EQ(true, info.waitUpdate());
        EQ(13, info.getInt());
    }

    void type_float()
    {
        DimService serv("TEST", 1.1f);
        EQ(0, serv.updateService(1.2f));

        DimInfoWaiter info("TEST", 0.1f);

        EQ(true, info.waitUpdate());
        EQ_DBL(1.2, info.getFloat(), 0.01);

        info.reset();
        EQ(1, serv.updateService(1.4f));
        EQ(true, info.waitUpdate());
        EQ_DBL(1.4, info.getFloat(), 0.01);
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestBasic);
