

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>
#include <boost/thread.hpp>
#include <boost/atomic.hpp>
#include <boost/chrono.hpp>
#include <string>

#include "dim.hxx"
#include "dis.hxx"
#include "dic.hxx"

#include "config.h"
#include "test_utils.hh"

#define EQ CPPUNIT_ASSERT_EQUAL
#define EQ_DBL CPPUNIT_ASSERT_DOUBLES_EQUAL

class TestDimInfo : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDimInfo);
    CPPUNIT_TEST(defaults);
    CPPUNIT_TEST_SUITE_END();

public:

    void tearDown()
    {
        dic_stop();
    }

    void defaults()
    {
        {
            DimInfo info("TEST", 42);
            EQ(42, info.getInt());
        }
        {
            DimInfo info("TEST", 42.4f);
            EQ_DBL(42.4, info.getFloat(), 0.01);
        }
        {
            DimInfo info("TEST", 42.4);
            EQ_DBL(42.4, info.getDouble(), 0.01);
        }
        {
            DimInfo info("TEST", 1LL);
            EQ(1LL, info.getLonglong());
        }
        {
            DimInfo info("TEST", static_cast<short>(1));
            EQ(static_cast<short>(1), info.getShort());
        }
        {
            DimInfo info("TEST", 1LL);
            EQ(1LL, info.getLonglong());
        }
        {
            std::string temp("test");
            DimInfo info("TEST", &temp[0]);
            EQ(std::string("test"), std::string(info.getString()));
        }
        {
            DimInfo info("TEST", static_cast<void *>(0), 0);
            EQ(static_cast<void *>(0), info.getData());
        }
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestDimInfo);
