
function(auto_option name desc default)
    if(NOT DEFINED default)
        set(default "AUTO")
    endif()
    if(NOT desc)
        set(desc "Enable ${MOPT_NAME} option")
    endif()


    set("${name}" "${default}" CACHE STRING "${desc}")
    set_property(CACHE "${name}" PROPERTY STRINGS "AUTO" ON OFF)
endfunction(auto_option)

function(set_option name value)
    set_property(CACHE "${name}" PROPERTY VALUE "${value}")
    message(STATUS "option ${name} is ${value}")
endfunction(set_option)
