
set(OS AUTO CACHE STRING "target platform")
set_property(CACHE OS PROPERTY STRINGS AUTO SunOS HP-UX OSF1 AIX LynxOS Linux Darwin)

option(THREADS "Threading support" ON)
option(BUILD_SHARED_LIBS "Build shared libraries" ON)

include(CMakeParseArguments)

function(find_extralib)
    cmake_parse_arguments(XLIB "" "" "PATHS" ${ARGN})
    find_library(TEMP ${XLIB_UNPARSED_ARGUMENTS} PATHS ${XLIB_PATHS})
    if(NOT TEMP)
        message(SEND_ERROR "Library not found: ${name}")
    endif(NOT TEMP)
    set(EXTRALIBS ${EXTRALIBS} ${TEMP} PARENT_SCOPE)
endfunction(find_extralib)

if(OS STREQUAL "AUTO")
    if(APPLE)
        set_option(OS "Darwin")
    elseif(UNIX)
        set_option(OS "Linux")
    else()
        message(WARNING "Failed to detect system type")
        set_option(OS "")
    endif()
endif(OS STREQUAL "AUTO")

set(ARCH_DEFS)
if(OS STREQUAL "SunOS")
    list(APPEND ARCH_DEFS MIPSEB PROTOCOL=1 sunos)
elseif(OS STREQUAL "Solaris")
    if(THREADS)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -mt")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mt")
        find_extralib("posix4")
    endif(THREADS)
    set(BUILD_SHARED_LIBS OFF)
    find_extralib("socket")
    find_extralib("nsl")
    list(APPEND ARCH_DEFS MIPSEB PROTOCOL=1 solaris)
elseif(OS STREQUAL "HP-UX")
    set(THREADS OFF)
    set(BUILD_SHARED_LIBS OFF)
    list(APPEND ARCH_DEFS MIPSEB PROTOCOL=1 hpux)
elseif(OS STREQUAL "OSF1")
    set(THREADS OFF)
    set(BUILD_SHARED_LIBS OFF)
    list(APPEND ARCH_DEFS MIPSEL PROTOCOL=1 osf)
elseif(OS STREQUAL "AIX")
    set(THREADS OFF)
    set(BUILD_SHARED_LIBS OFF)
    list(APPEND ARCH_DEFS MIPSEB PROTOCOL=1 unix _BSD aix)
elseif(OS STREQUAL "LynxOS")
    set(BUILD_SHARED_LIBS OFF)
    if(CMAKE_SYSTEM_PROCESSOR STREQUAL "RAID")
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -I/usr/include/bsd -I/usr/include/posix")
        list(APPEND ARCH_DEFS MIPSEB PROTOCOL=1 unix LYNXOS RAID)
        find_extralib("bsd" PATHS "/usr/posix/usr/lib")
    elseif(CMAKE_SYSTEM_PROCESSOR STREQUAL "INTEL")
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -mthreads")
        list(APPEND ARCH_DEFS MIPSEL PROTOCOL=1 unix LYNXOS mthreads)
        find_extralib("bsd")
    else()
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -mthreads")
        list(APPEND ARCH_DEFS MIPSEB PROTOCOL=1 unix LYNXOS mthreads)
        find_extralib("bsd")
    endif()
elseif(OS STREQUAL "Linux")
    # CMake will automatically add fPIC
    if(CMAKE_SYSTEM_PROCESSOR STREQUAL "ppc")
        list(APPEND ARCH_DEFS MIPSEB PROTOCOL=1 unix linux)
    else()
        list(APPEND ARCH_DEFS MIPSEL PROTOCOL=1 unix linux)
    endif()
elseif(OS STREQUAL "Darwin")
    # CMake will automatically add fPIC
    list(APPEND ARCH_DEFS MIPSEL PROTOCOL=1 unix linux darwin)
else()
    message(FATAL_ERROR "Unknown OS, please set using cmake -DOS=...")
endif()

if(THREADS)
    find_extralib("pthread")
else(THREADS)
    list(APPEND ARCH_DEFS NOTHREADS)
endif(THREADS)

foreach(ARCH_DEF IN LISTS ARCH_DEFS)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -D${ARCH_DEF}")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D${ARCH_DEF}")
endforeach()
